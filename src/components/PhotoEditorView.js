import React, { Component } from 'react';
import { requireNativeComponent } from 'react-native';

class PhotoEditorView extends Component {
  render() {
    return <RCTAdobeCreativeSDK />
  }
}

PhotoEditorView.propTypes = { }

var RCTAdobeCreativeSDK = requireNativeComponent('RCTAdobeCreativeSDK', PhotoEditorView);

module.exports = PhotoEditorView;
