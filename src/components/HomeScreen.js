import React, { Component } from 'react';
import { View, Button, NativeModules, NativeEventEmitter, Image, StyleSheet } from 'react-native';

const { AdobeCreativeSDKManager } = NativeModules;

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.eventEmitter;
    this.state = { imgSrc: 'https://facebook.github.io/react/img/logo_og.png' };
  }
  componentWillMount() {
    this.eventEmitter = new NativeEventEmitter(AdobeCreativeSDKManager);
    this.eventEmitter.addListener(AdobeCreativeSDKManager.PHOTO_EDITOR_FINISHED,
      (eventData) => {
        console.log(eventData);
        let imgSrc = 'data:image/png;base64,' + eventData.imgBase64;
        this.setState({ imgSrc: imgSrc })
      }
    );
  }
  componentWillUnmount() {
    this.eventEmitter.remove();
  }

  async buttonPress() {
    try {
      await AdobeCreativeSDKManager.presentEditorViewController(1);
    } catch (e) {
      console.error(e);
    }
  }

  render() {
      return (
        <View>
          <Image source={{ uri: this.state.imgSrc }} style={styles.image} />
          <Button
          onPress={() => {
            this.buttonPress();
          }}
          title="Show Editor via brigedmodule"
          color="#841584"
          />
        </View>
      );
  }
}

const styles = StyleSheet.create({
    image: {
        flex: 1,
        resizeMode: 'contain'
    }
});
export default HomeScreen;
