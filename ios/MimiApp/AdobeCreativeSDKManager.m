//
//  AdobeCreativeSDKModule.m
//  MimiApp
//
//  Created by Darren Lai on 3/15/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "AdobeCreativeSDKManager.h"
#import <React/RCTUIManager.h>
#import <React/UIView+React.h>

@implementation AdobeCreativeSDKManager
{
  AdobeUXImageEditorViewController *_controller;
  bool hasListeners;
}
static NSString *const kAVYAdobeCreativeCloudKey = @"8e82424f18334c09abd5726eb106e37f";
static NSString *const kAVYAdobeCreativeCloudSecret = @"59c7261f-4a81-4a7c-be83-3a666a6ccc81";

- (instancetype)init
{
  self = [super init];
  if (self) {
    RCTLog(@"RCTAdobeCreativeSDKManager init");
    [[AdobeUXAuthManager sharedManager] setAuthenticationParametersWithClientID:kAVYAdobeCreativeCloudKey
                                                               withClientSecret:kAVYAdobeCreativeCloudSecret];
  }
  return self;
}

- (NSDictionary *)constantsToExport
{
  return @{ @"PHOTO_EDITOR_CANCELED": @"onPhotoEditorCanceled", @"PHOTO_EDITOR_FINISHED" : @"onPhotoEditorFinishedWithImage" };
}

-(NSArray<NSString *> *)supportedEvents
{
  return @[@"onPhotoEditorCanceled", @"onPhotoEditorFinishedWithImage"];
}

-(void)startObserving
{
  hasListeners = YES;
}

-(void)stopObserving
{
  hasListeners = NO;
}

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD
(
 presentEditorViewController:(nonnull NSNumber *)reactTag
 resolver:(RCTPromiseResolveBlock)resolve
 rejecter:(RCTPromiseRejectBlock)reject
 )
{
  UIImage *image = [UIImage imageNamed:@"marchNewItems"];
  _controller = [[AdobeUXImageEditorViewController alloc] initWithImage:image];
  [_controller setDelegate:self];
  
  RCTUIManager *uiManager = self.bridge.uiManager;
  dispatch_async(uiManager.methodQueue, ^{
    [uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
      UIView *view = viewRegistry[reactTag]; // tag 1 is the rootView
      UIViewController *viewController = (UIViewController *)view.reactViewController;
      [viewController presentViewController:_controller animated:YES completion:^{}];
      resolve(@"presentation done");
    }];
  });
}



- (void)photoEditorCanceled:(AdobeUXImageEditorViewController *)editor
{
  __weak typeof(self) weakSelf = self;
  [editor dismissViewControllerAnimated:YES completion:^{
    if (weakSelf) {
      __strong typeof(weakSelf) strongSelf = weakSelf;
      if (strongSelf->hasListeners) {
        [strongSelf sendEventWithName:@"onPhotoEditorCanceled" body:nil];
      }
    }
  }];
  
}

- (void)photoEditor:(AdobeUXImageEditorViewController *)editor finishedWithImage:(UIImage *)image
{
  NSString *base64Encoded = nil;
  if (image != nil) {
    NSData *imageData = UIImagePNGRepresentation(image);
    base64Encoded = [imageData base64EncodedStringWithOptions:0];
  }
  __weak typeof(self) weakSelf = self;
  [editor dismissViewControllerAnimated:YES completion:^{
    if (weakSelf) {
      __strong typeof(weakSelf) strongSelf = weakSelf;
      if (strongSelf->hasListeners && base64Encoded != nil) {
        [strongSelf sendEventWithName:@"onPhotoEditorFinishedWithImage" body:@{@"imgBase64": base64Encoded}];
      }
    }
  }];
}
@end
