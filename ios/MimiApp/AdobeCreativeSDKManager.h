//
//  AdobeCreativeSDKModule.h
//  MimiApp
//
//  Created by Darren Lai on 3/15/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <AdobeCreativeSDKCore/AdobeCreativeSDKCore.h>
#import <AdobeCreativeSDKImage/AdobeCreativeSDKImage.h>

@interface AdobeCreativeSDKManager : RCTEventEmitter<RCTBridgeModule, AdobeUXImageEditorViewControllerDelegate>

@end
